'''
Tests
'''
from nose.tools import assert_equal


class TestA(object):
    '''Test class'''
    @classmethod
    def setup_class(cls):
        '''per class'''
        pass

    @classmethod
    def teardown_class(cls):
        '''per class'''
        pass

    def setUp(self):
        '''per test'''
        pass

    def teardown(self):
        '''per test'''
        pass

    def test_sameple(self):
        '''
        Sample test
        '''
        assert_equal(1, 1)
