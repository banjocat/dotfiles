#source /usr/local/bin/virtualenvwrapper.sh
#for mac
source /usr/local/bin/virtualenvwrapper.sh

alias code='cd ~/code'
alias work='cd ~/work'
alias vi='vim'
alias sudo='sudo -H'
alias m='meld . &'

export PS1="\w> "
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacadk

# Gets templates of common working enviroments
function get_template {
  svn export --force https://github.com/banjocat/dotfiles.git/trunk/templates/$1/ .
  rm -rf .svn
}
