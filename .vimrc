" put this line first in ~/.vimrc
set nocompatible | filetype indent plugin on | syn on

fun! SetupVAM()
  let c = get(g:, 'vim_addon_manager', {})
  let g:vim_addon_manager = c
  let c.plugin_root_dir = expand('$HOME', 1) . '/.vim/vim-addons'

  " Force your ~/.vim/after directory to be last in &rtp always:
  " let g:vim_addon_manager.rtp_list_hook = 'vam#ForceUsersAfterDirectoriesToBeLast'

  " most used options you may want to use:
  " let c.log_to_buf = 1
  " let c.auto_install = 0
  let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'
  if !isdirectory(c.plugin_root_dir.'/vim-addon-manager/autoload')
    execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '
        \       shellescape(c.plugin_root_dir.'/vim-addon-manager', 1)
  endif

  " This provides the VAMActivate command, you could be passing plugin names, too
  call vam#ActivateAddons([], {})
endfun
call SetupVAM()

" ACTIVATING PLUGINS

" Need to compile YouCompleteMe - run ./install.py within the plugin directory
VAMActivate The_NERD_tree Emmet
VAMActivate vim-javascript vim-auto-save
VAMActivate vim-go vim-less@groenwege html5 surround 
VAMActivate ctrlp Syntastic fugitive vim-multiple-cursors awk
VAMActivate YouCompleteMe Supertab
VAMActivate UltiSnips vim-autopep8





syntax on
filetype off
filetype on               
filetype plugin on               
filetype plugin indent on        
set nocompatible  
set autoindent                   
set expandtab                    
set cmdheight=2                  
set showmatch
set shiftwidth=4
set softtabstop=4
set wildmenu
set wildmode=list:longest,full
set number
set hlsearch
set showcmd
set ruler
set autoread
set history=200
set undolevels=1000
set ttyfast
set noerrorbells
set background=dark
set backspace=indent,eol,start
let g:clipbrdDefaultReg = '+'
set backupdir=~/.swaps
set directory=~/.swaps
" MAPS
inoremap jj <esc>
noremap <tab><tab> <C-w><C-w>
"Open tag in newtab
inoremap <C-@> _

"gui options
set guioptions=aeimrL
set guifont=Monospace\ 16
set t_Co=256

"TAB MAPS
noremap <F1> 1gt 
noremap <F2> 2gt 
noremap <F3> 3gt 
noremap <F4> 4gt 
noremap <F5> 5gt 
noremap <F6> 6gt 
noremap <F7> 7gt 
noremap <F8> 8gt 
noremap <F9> 9gt 

inoremap <F1> <C-O>1gt
inoremap <F2> <C-O>2gt
inoremap <F3> <C-O>3gt
inoremap <F4> <C-O>4gt
inoremap <F5> <C-O>5gt
inoremap <F6> <C-O>6gt
inoremap <F7> <C-O>7gt
inoremap <F8> <C-O>8gt
inoremap <F9> <C-O>9gt

set tags=./tags;/

colorscheme default
noremap 's :UltiSnipsEdit<CR>
noremap <C-\> :NERDTreeToggle %<CR>
noremap 'c :Tagbar<CR>
noremap <C-?> :nohl<CR>

let g:NERDTreeDirArrows=0
set encoding=utf-8

let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_autoclose_preview_window_after_completion = 1
let g:SuperTabDefaultCompletionType = '<C-n>'
let g:ycm_server_use_vim_stdout = 1


let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsListSnippets="<c-e>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsUsePythonVersion = 2
let g:UltiSnipsEditSplit='vertical'
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/UltiSnips']

let g:syntastic_check_on_open = 1 " Check on open
let g:syntastic_enable_highlighting = 0 " Disable highlighting

let g:auto_save = 1
let g:auto_save_in_insert_mode = 0  " do not save while in insert mode


au FileType python setlocal formatprg=autopep8\ -


autocmd FileType javascript vnoremap <buffer>  <c-f> :call RangeJsBeautify()<cr>
autocmd FileType json vnoremap <buffer> <c-f> :call RangeJsonBeautify()<cr>
autocmd FileType jsx vnoremap <buffer> <c-f> :call RangeJsxBeautify()<cr>
autocmd FileType html vnoremap <buffer> <c-f> :call RangeHtmlBeautify()<cr>
autocmd FileType css vnoremap <buffer> <c-f> :call RangeCSSBeautify()<cr>


