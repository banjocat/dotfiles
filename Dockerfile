FROM ubuntu

RUN apt-get update
RUN apt-get install -y subversion 
RUN apt-get install -y vim 
RUN apt-get install -y sshfs 
RUN apt-get install -y build-essential 
RUN apt-get install -y git 
RUN apt-get install -y git-svn
RUN apt-get install -y cmake
RUN apt-get install -y curl
RUN apt-get install -y python-dev
RUN apt-get install -y openssh-server
RUN apt-get install -y python-pip
RUN apt-get install -y nodejs
RUN apt-get install -y npm
RUN apt-get install -y cscope
RUN apt-get install -y ctags

RUN cp /root/.bashrc /root/.bashrc.origin
RUN mkdir ~/.swaps

ADD .bashrc /root/.bashrc
ADD .vimrc /root/.vimrc

